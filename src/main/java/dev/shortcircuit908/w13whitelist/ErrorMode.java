package dev.shortcircuit908.w13whitelist;

public enum ErrorMode {
	SILENT,
	DEFAULT_WHITELIST,
	LOCKDOWN;
	
	public static ErrorMode parse(String mode){
		mode = mode.replaceAll("[\\-_]", "");
		for(ErrorMode check_mode : ErrorMode.values()){
			if(check_mode.name().replaceAll("[\\-_]", "").equalsIgnoreCase(mode)){
				return check_mode;
			}
		}
		return LOCKDOWN;
	}
}
