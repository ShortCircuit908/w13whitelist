package dev.shortcircuit908.w13whitelist;

import dev.shortcircuit908.w13discordlink.W13DiscordLink;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.plugin.ApiVersion;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.LogPrefix;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

@Plugin(name = "w13whitelist", version = "2.1.2")
@Author("ShortCircuit908")
@ApiVersion(ApiVersion.Target.v1_15)
@Description("Discord role-based whitelisting")
@LogPrefix("W13WL")
@Dependency("w13discordlink")
public class W13Whitelist extends JavaPlugin {
	private final Config config = new Config();
	private W13DiscordLink discord_link;
	
	@Override
	public void onEnable() {
		discord_link = getPlugin(W13DiscordLink.class);
		
		saveDefaultConfig();
		reloadConfig();
		
		if (getCustomConfig().getGuildSnowflake() == null || getCustomConfig().getGuildSnowflake().trim().isEmpty()) {
			String transfer = discord_link.getConfig().getString("guild-snowflake");
			if(transfer != null){
				getLogger().info("Copying guild-snowflake from " + discord_link.getName());
				getConfig().set("guild-snowflake", transfer);
				saveConfig();
			}
			else {
				getLogger().severe("Discord guild snowflake has not been set");
				getLogger().severe("Member roles will not be checked, and the whitelist will operate in " +
						getCustomConfig().getErrorMode() + " mode");
			}
		}
		
		if(getConfig().get("role-permissions", null) == null){
			ConfigurationSection section = discord_link.getConfig().getConfigurationSection("role-permissions");
			Map<String, Object> copied;
			if(section != null && !(copied = section.getValues(true)).isEmpty()){
				getLogger().info("Copying role-permissions from " + discord_link.getName());
				getConfig().set("role-permissions", copied);
				saveConfig();
			}
		}
		
		getServer().getPluginManager().registerEvents(new WhitelistListener(this), this);
	}
	
	@Override
	public void onDisable() {
	
	}
	
	@Override
	public void onLoad() {
		// Move existing databases, if present
		Path link_file = getDataFolder().toPath().resolve("whitelist.db");
		Path cache_file = getDataFolder().toPath().resolve("connected_cache.db");
		
		File target_file = getPlugin(W13DiscordLink.class).getDataFolder();
		Path target_path = target_file.toPath();
		
		Path link_file_target = target_path.resolve("links.db");
		Path cache_file_target = target_path.resolve("connected_cache.db");
		
		if (!target_file.exists()) {
			target_file.mkdirs();
		}
		
		moveFile(link_file, link_file_target);
		moveFile(cache_file, cache_file_target);
	}
	
	private void moveFile(Path source, Path target) {
		if (source.toFile().exists()) {
			if (target.toFile().exists()) {
				getLogger().warning("Cannot move " + source + " to " + target + ": File exists");
			}
			else {
				try {
					Files.move(source, target);
				}
				catch (IOException e) {
					e.printStackTrace();
					getLogger().warning("Failed to move " + source + " to " + target);
				}
			}
		}
	}
	
	public W13DiscordLink getDiscordLink() {
		return discord_link;
	}
	
	public Config getCustomConfig() {
		return config;
	}
	
	public class Config {
		public String getGuildSnowflake() {
			return getConfig().getString("guild-snowflake");
		}
		
		public Map<String, List<String>> getRolePermissions() {
			ConfigurationSection section = getConfig().getConfigurationSection("role-permissions");
			if (section == null) {
				return new HashMap<>(0);
			}
			Set<String> roles = section.getKeys(false);
			return roles.stream().collect(Collectors.toMap(Function.identity(), section::getStringList));
		}
		
		public List<String> getWhitelistedRoles() {
			return getConfig().getStringList("whitelisted-roles");
		}
		
		public ErrorMode getErrorMode() {
			return ErrorMode.parse(getConfig().getString("error-handling"));
		}
		
		public String getMessage(String id) {
			id = "messages." + id;
			String message = getConfig().getString(id);
			return ChatColor.translateAlternateColorCodes('&', message == null ? id : message);
		}
	}
}
