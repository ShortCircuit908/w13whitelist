package dev.shortcircuit908.w13whitelist;

import dev.shortcircuit908.w13discordlink.DiscordBot;
import dev.shortcircuit908.w13discordlink.event.AsyncDiscordPreLoginEvent;
import dev.shortcircuit908.w13discordlink.event.DiscordJoinEvent;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.Permissible;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class WhitelistListener implements Listener {
	private final W13Whitelist plugin;
	private final HashMap<UUID, WhitelistResult> whitelist_results = new HashMap<>();
	private static final TemporalAmount cache_expire = Duration.of(5, ChronoUnit.MINUTES);
	
	public WhitelistListener(W13Whitelist plugin) {
		this.plugin = plugin;
	}
	
	private static class WhitelistResult {
		private final Instant timestamp;
		private boolean allowed;
		private String message;
		private final Set<String> roles;
		
		public WhitelistResult(boolean allowed, String message) {
			this(allowed, message, new HashSet<>(0));
		}
		
		public WhitelistResult(boolean allowed, String message, Set<String> roles) {
			this.allowed = allowed;
			this.message = message;
			this.roles = roles;
			this.timestamp = Instant.now();
		}
	}
	
	@EventHandler
	public void onPreLogin(final AsyncDiscordPreLoginEvent event) {
		WhitelistResult result = new WhitelistResult(false, null);
		try {
			Set<String> roles = plugin.getDiscordLink()
					.getBot()
					.getMemberRoles(plugin.getCustomConfig().getGuildSnowflake(), event.getUserSnowflake())
					.orElse(new HashSet<>(0));
			result = checkWhitelistedRole(roles);
		}
		catch (DiscordBot.InvalidGuildException e) {
			plugin.getLogger().severe("Invalid guild snowflake");
			result.allowed = false;
			result.message = plugin.getCustomConfig().getMessage("errors.role-missing");
		}
		
		if (!result.allowed) {
			event.getParent().setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_WHITELIST);
			if (result.message != null) {
				event.getParent().setKickMessage(result.message);
			}
		}
		whitelist_results.put(event.getParent().getUniqueId(), result);
	}
	
	@EventHandler
	public void onJoin(final DiscordJoinEvent event) {
		UUID player_id = event.getParent().getPlayer().getUniqueId();
		Optional<WhitelistResult> result_opt = getCachedResult(player_id);
		if (result_opt.isPresent()) {
			WhitelistResult result = result_opt.get();
			if (!result.allowed && !event.getParent().getPlayer().isOp()) {
				event.getParent().getPlayer().kickPlayer(result.message);
				return;
			}
			Map<String, List<String>> role_permissions = plugin.getCustomConfig().getRolePermissions();
			applyPermission(event.getParent().getPlayer(), result.roles, role_permissions);
			return;
		}
		WhitelistResult result = new WhitelistResult(false, null);
		try {
			Set<String> roles = plugin.getDiscordLink()
					.getBot()
					.getMemberRoles(plugin.getCustomConfig().getGuildSnowflake(), event.getUserSnowflake())
					.orElse(new HashSet<>(0));
			result = checkWhitelistedRole(roles);
			Map<String, List<String>> role_permissions = plugin.getCustomConfig().getRolePermissions();
			applyPermission(event.getParent().getPlayer(), roles, role_permissions);
		}
		catch (DiscordBot.InvalidGuildException e) {
			plugin.getLogger().severe("Invalid guild snowflake");
			result.allowed = false;
			result.message = plugin.getCustomConfig().getMessage("errors.role-missing");
		}
		if (!result.allowed) {
			event.getParent().getPlayer().kickPlayer(result.message);
		}
		whitelist_results.put(player_id, result);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onJoin(final PlayerJoinEvent event) {
		// Defer checking for about half a second. Hopefully long enough to let the DiscordEvents
		// go through, but not long enough to let troublemakers pull any funny business
		final int delay_ticks = 10;
		final Player player = event.getPlayer();
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
			Optional<WhitelistResult> result_opt = getCachedResult(player.getUniqueId());
			if (!player.isOp() && !result_opt.isPresent()) {
				WhitelistResult result = new WhitelistResult(false, null);
				handleWhitelistError(player.getUniqueId(), result);
				if (!result.allowed) {
					player.kickPlayer(result.message);
				}
			}
		}, delay_ticks);
	}
	
	@EventHandler
	public void onQuit(final PlayerQuitEvent event) {
		whitelist_results.remove(event.getPlayer().getUniqueId());
	}
	
	private WhitelistResult checkWhitelistedRole(Set<String> user_roles) {
		final WhitelistResult result =
				new WhitelistResult(false, plugin.getCustomConfig().getMessage("role-missing"), user_roles);
		List<String> whitelisted_roles = plugin.getCustomConfig().getWhitelistedRoles();
		
		for (String role : user_roles) {
			if (whitelisted_roles.contains(role)) {
				result.allowed = true;
				break;
			}
		}
		
		return result;
	}
	
	private void handleWhitelistError(UUID player_id, WhitelistResult result) {
		// Get preferred error mode
		ErrorMode error_mode = plugin.getCustomConfig().getErrorMode();
		plugin.getLogger().warning("Handling player " + player_id + " in error mode: " + error_mode);
		switch (error_mode) {
			case DEFAULT_WHITELIST:
				// In default-whitelist mode, defer control to the server's built-in whitelist
				Set<OfflinePlayer> whitelisted = plugin.getServer().getWhitelistedPlayers();
				for (OfflinePlayer whitelisted_player : whitelisted) {
					if (whitelisted_player.getUniqueId().equals(player_id)) {
						plugin.getLogger().warning(player_id + " is on the default server whitelist");
						result.allowed = true;
						return;
					}
				}
				plugin.getLogger().warning(player_id + " is not on the default server whitelist");
				result.allowed = false;
				result.message = plugin.getCustomConfig().getMessage("errors.default-whitelist");
				break;
			case SILENT:
				break;
			default:
				result.allowed = false;
				result.message = plugin.getCustomConfig().getMessage("errors.lockdown");
		}
	}
	
	private void applyPermission(Permissible permissible, Set<String> roles,
								 Map<String, List<String>> role_permissions) {
		Set<String> visited = new HashSet<>();
		for (String role : roles) {
			applyPermission(permissible, role, role_permissions, visited);
		}
	}
	
	private void applyPermission(Permissible permissible, String role, Map<String, List<String>> role_permissions,
								 Set<String> visited) {
		// If this role has already been applied, return early to prevent circular cascading
		if (!visited.add(role)) {
			return;
		}
		List<String> permissions = role_permissions.get(role);
		if (permissions == null) {
			return;
		}
		for (String permission : permissions) {
			// Allow role cascading
			if (permission.startsWith("#")) {
				applyPermission(permissible, permission.substring(1), role_permissions, visited);
			}
			else {
				// Check for a negated permission
				boolean negated = permission.startsWith("-");
				permissible.addAttachment(plugin, negated ? permission.substring(1).trim() : permission, !negated);
			}
		}
	}
	
	private Optional<WhitelistResult> getCachedResult(UUID key) {
		if (!whitelist_results.containsKey(key)) {
			return Optional.empty();
		}
		WhitelistResult result = whitelist_results.get(key);
		if (result == null || result.timestamp.plus(cache_expire).isBefore(Instant.now())) {
			return Optional.empty();
		}
		return Optional.of(result);
	}
}
